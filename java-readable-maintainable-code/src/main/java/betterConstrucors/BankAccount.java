package betterConstrucors;

public class BankAccount {
    private double balance;
    private double interest;

    BankAccount(){}

    BankAccount(double balance, double interest){
        if(interest < 0)
            throw new IllegalArgumentException("Interest rate cannot less than 0");
        if(balance < 0)
            throw new IllegalArgumentException("Starting balance cannot less than 0");
        this.balance = balance;
        this.interest = interest;
    }
}
