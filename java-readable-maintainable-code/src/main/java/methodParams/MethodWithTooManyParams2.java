package methodParams;

public class MethodWithTooManyParams2 {
    String greeting = new EmailSender().constructTemplateEmail(new Person("Mr", "John", "Mr"));

}

class EmailSender {
    EmailSender(){}

    public String constructTemplateEmail(Person person){
        return "";
    }


}

