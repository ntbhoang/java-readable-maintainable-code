package methodParams;

public class FlagArgument {
    /*
        Flag argument are ugly.
        It immediately complicates the signature of the method, loudly proclaiming that this function does more than 1 thing
        -- Robert Martin
     */

    private static void switchLights(String room, boolean opt){

    }
    // Should be split into
    private static void switchLightsOn(String room){}
    private static void switchLightsOff(String room){}

}
