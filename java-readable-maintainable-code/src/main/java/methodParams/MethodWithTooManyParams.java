package methodParams;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class MethodWithTooManyParams {
    public static void main(String[] args) {
        // before
        long millis = nowPlusTime(3, 1, 4);

        // after
        long dayMillis = nowPlusDays(4);
    }


    private static long nowPlusTime(int months, int weeks, int days){
        return LocalDateTime.now().plusMonths(months)
                .plusWeeks(weeks)
                .plusDays(days)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }
    // Split into 3 methods
    private static long nowPlusMonths(int months){
        return LocalDateTime.now().plusMonths(months)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

    private static long nowPlusWeeks(int weeks){
        return LocalDateTime.now().plusWeeks(weeks)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

    private static long nowPlusDays(int days){
        return LocalDateTime.now().plusDays(days)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

}
