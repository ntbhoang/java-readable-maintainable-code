package methodParams;

public class Person {

    private String title;
    private String lastName;
    private String firstName;

    Person(String title, String firstName, String lastName) {
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Object getName() {
        return this.firstName;
    }

    public boolean getTitle() {
        this.title = title;
        return false;
    }

    public int getPin() {
        return 1;
    }

    public String getPerSonInfo() {
        return "";
    }

    // this constructor with 3 params can be fixed with Builder pattern

}
