package implementingMethods.refactorDuplication;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class MethodWithTooManyParams {
    public static void main(String[] args) {
        // before
        //long millis = nowPlusTime(3, 1, 4);

        // after
    }

    private static long toEpochMilli(LocalDateTime time){
        return time.atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

    private static void checkTimeIsValid(int time){
        if(time <= 0) throw new IllegalArgumentException("Time Unit cannot be <= 0");
    }

    // Split into 3 methods
    private static long nowPlusMonths(int months){
        checkTimeIsValid(months);
        return toEpochMilli(LocalDateTime.now().plusMonths(months));
    }

    private static long nowPlusWeeks(int weeks){
        checkTimeIsValid(weeks);
        return toEpochMilli((LocalDateTime.now().plusWeeks(weeks)));
    }

    private static long nowPlusDays(int days){
        return LocalDateTime.now().plusDays(days)
                .atZone(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

}
