package implementingMethods;

import javax.naming.InsufficientResourcesException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Examples {
    public static void main(String[] args) {

        // resulting balance is -1?
        // or does -1 have a special meaning
        if(withDraw(100) == -1){
            // do something
        }

    }

    private List<String> getDataFromMongoDb(){
        try {
            // some logic here
        } catch(Exception e){
            // operation failed
            return Collections.emptyList();
            // return null;
        }
        return new ArrayList<>();
    }

    private static int withDraw(int amount){
        int balance = 10000;
        if(amount > balance) return -1;
        else {
            balance -= amount;
            return 0;
        }
    }

    private static void withDrawBetter(int amount) throws InsufficientFundsException {
        int balance = 10000;
        if(amount > balance)
            throw new InsufficientFundsException();
        balance -= amount;
    }

    private static class InsufficientFundsException extends Exception {
    }
}
