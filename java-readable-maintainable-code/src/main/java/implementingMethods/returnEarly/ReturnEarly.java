package implementingMethods.returnEarly;

import methodParams.Person;

public class ReturnEarly {
    private boolean systemIsUp;
    public String getPersonInfo(Person person, int pin){
        if(systemIsUp){
            if(person != null && person.getName().equals("")){
                if(person.getPin() != pin){
                    return "Invalid pin";
                }
                return "Invalid Name";
            }
            return  "System is down";
        }
        return person.getPerSonInfo();
    }

    public String getPersonInfo2(Person person, int pin){
        if(systemIsUp) return "System is down";
        if(person != null && person.getName().equals("")) return "invalid";
        if(person.getPin() != pin) return "invalid pin";

        return person.getPerSonInfo();
    }
}
