package handlingException;

public class CatchException {
    public static void main(String[] args) {

        try{
            isAdultBetterCatch(-1);
        }catch (IllegalArgumentException ex){
            ex.printStackTrace();
        }

        // When you run this code - what you will observe is the simple stack trace which tells nothing
        // It better is logging useful information inside the `catch` block
    }


    private static void isAdult(int age) throws IllegalArgumentException{
        if(age < 0)
            throw new IllegalArgumentException("Invalid Age!!");
    }

    private static void isAdultBetterCatch(int age) throws IllegalArgumentException{
        if(age < 0)
            throw new IllegalArgumentException("The Age should be at least 0. Actual value passed in " + age);
    }
}
